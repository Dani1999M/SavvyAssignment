<?php
	
	class RaceVehicles
	{
		private array $players;
		
		public function __construct(array $players)
		{
			$this->players = $players;
		}
		
		public function runRace(int $distance): array
		{
			$results = [];
			
			foreach ($this->players as $player) {
				$time = $distance / $player->getSelectedVehicle()->getMaxSpeed();
				$results[$player->getName()] = $time;
			}
			
			return $results;
		}
		
		public function determineWinner(array $results): string
		{
			return "winner " . array_search(min($results), $results) . "!";
		}
	}