<?php
	require_once 'Models/Vehicle.php';
	require_once 'Models/Player.php';
	require_once 'Models/RaceVehicles.php';
	$vehiclesData = json_decode(file_get_contents('vehicles.json'), true);
	$vehicles = [];
	foreach ($vehiclesData as $vehicleData) {
		$vehicles[] = new Vehicle($vehicleData['name'], $vehicleData['maxSpeed']);
	}
	$daniel = new Player('Daniel');
	$json = new Player('Json');
	
	$daniel->selectVehicle($vehicles[0]);
	$json->selectVehicle($vehicles[1]);
	
	$race = new RaceVehicles([$daniel, $json]);
	
	$results = $race->runRace(100);
	
	echo $race->determineWinner($results) . PHP_EOL;
	
	foreach ($results as $player => $time) {
		echo "$player finished in $time seconds." . PHP_EOL;
	}